extends Node2D

@export_category("Effects")
@export_enum("dust", "pschit", "hit", "boom", "impact", "ink", "smoke",
			"fire","explosion", "splash", "buzz") var type: String = "dust"
@export var index: int = 0
@export var onetime: bool = true
@onready var g_anim = $animation/def

var g_max = { "dust": 2, "pschit": 2, "hit": 3, "boom": 1, "impact": 1,
			"ink": 1, "smoke": 1, "fire": 1, "explosion": 5, "splash": 3,
			"buzz": 1}

#========= SIGNAL ==============================================================
signal on_finished()

#========= ACCESSORS ===========================================================
func set_type(_type, _index) : type = _type ; index = _index
func set_sprite_modulate(_color): $sprite.set_modulate(_color)

#========= INITIALISATION ======================================================
func _ready():
	visible = false
	if onetime: start()
	
func set_flip_h(_val):
	$sprite/s_32.flip_h =_val ; $sprite/s_48.flip_h =_val
	$sprite/s_64.flip_h =_val ; $sprite/s_96.flip_h =_val
	
func set_flip_v(_val):
	$sprite/s_32.flip_v =_val ; $sprite/s_48.flip_v =_val
	$sprite/s_64.flip_v =_val ; $sprite/s_96.flip_v =_val

#========= RUN FX ACCORDING TO ITS TYPE ========================================
func start():
	visible = true
	var v_type = type
	var v_max = g_max[type]		
	if index==0 : index = 1+randi()%v_max
	v_type=v_type+"0"+str(clamp(index,1,v_max))
	g_anim.play(v_type)

#========= PROCESS =============================================================
func _process(_delta): pass

#========= DO DESTRUCTOR OR WAIT ANOTHER START =================================
func _on_finished(_anim_name):
	# emit_signal("on_finished")
	if onetime : 	queue_free()
	else :			visible = false
