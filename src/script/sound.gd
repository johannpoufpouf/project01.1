extends Node2D

var g_force = 5
func set_force(_force):		g_force = _force


func _ready():
	pass # Replace with function body.

func on_area_entered(area):
	area.owner.on_hit(GLOBAL.WEAPON.SOUND, g_force, position)

func _on_hit_1_area_entered(area): on_area_entered(area)
func _on_hit_2_area_entered(area): on_area_entered(area)
func _on_hit_3_area_entered(area): on_area_entered(area)

