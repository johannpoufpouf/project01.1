extends Area2D

var g_wait_time = 0
var g_offset = Vector2.ZERO
var g_force = 5

func _ready():
	visible=false
	if g_wait_time :
		$timer.wait_time = g_wait_time
		$timer.start()
	else:
		_on_timer_timeout()

func set_timer(_delay):		g_wait_time = _delay
func set_offset(_offset):	g_offset = _offset
func set_force(_force):		g_force = _force

func _on_timer_timeout():
	if GLOBAL.PLAYER!=null: position = GLOBAL.PLAYER.position+g_offset
	$def.play("run")


func _on_area_entered(area): area.owner.on_hit(GLOBAL.WEAPON.HOOK, g_force, position)
