extends Control

@export_category("DebugKeys")
@export_enum("Dump_tree") var key_b: String = "Dump_tree"
@export_enum("Dump_tree") var key_n: String = "Dump_tree"

#========= INITIALISATION ======================================================
func _ready(): pass

#========= DEBUG KEYS ==========================================================
func _process(_delta):
	$general.text = "NB NODES: "+str(get_tree().get_node_count()) + \
					" FPS: "+str(Engine.get_frames_per_second())
	
	$people.text = "FOES: "+str(GLOBAL.nb_foes_alive)+" DEAD: "+str(GLOBAL.nb_foes_dead)
	
	var keys = [{"key":"ui_debug_1","value":key_b},
				{"key":"ui_debug_2","value":key_n}]
	for i in keys.size() :
		if GLOBAL.PLAYER && Input.is_action_just_pressed(keys[i].key) :
			match keys[i].value :
				"Dump_tree":		get_tree().get_root().print_tree_pretty()

#========= CONNET TO PLAYER ====================================================
func connect_to_player():
	print("[DBG] connect to player ("+str(GLOBAL.PLAYER)+")")
	if GLOBAL.PLAYER:
		GLOBAL.PLAYER.connect("pingmy_position", _on_pingmy_position)

#========= PLAYER POSITION =====================================================
func _on_pingmy_position(_pos): $position.text = "POS: "+str(_pos)

#========= PLAYER STATISTIC UPDATE =============================================
func _on_pingmy_update_stats(_what, _data):
	var v_text = _what.to_upper()+": "+str(_data)
	print("[DBG] "+v_text) ; $stats.text = v_text
	
