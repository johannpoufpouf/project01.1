extends Area2D

var g_force = 5
var g_nb_hit = 1
var g_dir = Vector2(1,0)
var g_speed= 150

var g_velocity = Vector2(1,0)

func set_force(_force):		g_force = _force
func set_nb_hit(_nb_hit):	g_nb_hit = _nb_hit
func set_dir(_dir):			g_dir = _dir
func set_speed(_speed):		g_speed = _speed

func _ready():
	$timeout.wait_time = 320/g_speed
	$timeout.start()
	g_velocity = g_dir*g_speed

func _process(delta): position = position+g_velocity*delta

func _on_area_entered(area):
	if g_nb_hit>0:
		area.owner.on_hit(GLOBAL.WEAPON.ROCK, g_force, position)
		g_nb_hit = g_nb_hit-1
		if (g_nb_hit<=0) : queue_free()


func _on_timeout_timeout(): queue_free()
