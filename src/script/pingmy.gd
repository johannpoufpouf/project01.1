extends CharacterBody2D

var fx_scene 		= preload("res://src/scene/fx.tscn")
var hook_scene 		= preload("res://src/scene/weapons/hook.tscn")
var rock_scene 		= preload("res://src/scene/weapons/rock.tscn")
var sound_scene		= preload("res://src/scene/weapons/sound.tscn")

signal pingmy_position(_pos)
signal pingmy_die()
signal pingmy_end()

@onready var g_animationtree	= $animation/tree
@onready var g_state			= g_animationtree["parameters/playback"]

@export_category("Weapons")
@export var hook: int = 0
@export var rock: int = 0
@export var sound:int = 0


var m_hook_delay = 0
var m_rock_delay = 0
var m_rock_side = true
var m_current_sound = 0

var g_side = 0


func travel(_value): if (g_state.get_current_node()!=_value): g_state.travel(_value)

#======== INITIALISATION =======================================================
func _ready():
	GLOBAL.set_player(self)		# PLAYER NODE IS AVAILABLE FROM GLOBAL

#======== PLAYER INPUTS ========================================================
func _physics_process(_delta):
	var v_input_dir	= Vector2(Input.get_axis("ui_left", "ui_right"), \
						Input.get_axis("ui_up", "ui_down")).normalized()

	var v_target = Vector2.ZERO
	var v_accel = 300
	if v_input_dir!=Vector2.ZERO :
		v_target = 80 * v_input_dir
		set_direction(v_input_dir)

	velocity = velocity.move_toward(v_target, v_accel*_delta)
	if (velocity!=Vector2.ZERO) : travel("run") ; move_and_slide()
	else: travel("idle")

	emit_signal("pingmy_position",position)
	
	if hook:
		m_hook_delay = m_hook_delay + _delta
		if (m_hook_delay>=2) :
			m_hook_delay = 0
			var v_nb_hooks = 4
			for i in v_nb_hooks:
				var v_hook = hook_scene.instantiate()
				var v_offset = -i*6 ; if i>=2: v_offset = v_offset+6
				v_hook.set_offset(Vector2(0,v_offset))
				v_hook.set_timer(i*0.15)
				v_hook.set_force(5)
				var v_flip = 1; if i%2==g_side: v_flip = -1
				var v_size = 0.8
				v_hook.scale=Vector2(v_flip*v_size,v_size)
				get_parent().add_child(v_hook)
				
	if rock:
		m_rock_delay = m_rock_delay + _delta
		if (m_rock_delay>1) :
			m_rock_delay = 0
			var v_nb_rocks = 8
			for i in v_nb_rocks:
				var v_rock = rock_scene.instantiate()
				v_rock.position = position-Vector2(0,5)
				v_rock.set_force(5)
				v_rock.set_nb_hit(3)
				v_rock.set_speed(150)
				var v_angle = 2*PI*i/v_nb_rocks
				if m_rock_side: v_angle=v_angle+PI/v_nb_rocks
				v_rock.set_dir(Vector2(1,0).rotated(v_angle))
				get_parent().add_child(v_rock)
				
			m_rock_side = ! m_rock_side
			
	if sound != m_current_sound:
		match m_current_sound:
			0:
				var v_sound = sound_scene.instantiate()
				v_sound.position = Vector2(0,10)
				v_sound.modulate = Color(1.0,1.0,1.0,0.5)
				var v_size = 1.1
				v_sound.scale=Vector2(v_size,v_size)
				$content/sprite/sound.add_child(v_sound)
			
		m_current_sound = m_current_sound + 1

func set_direction(_dir):
	g_animationtree.set("parameters/run/blend_position", _dir)
	g_animationtree.set("parameters/idle/blend_position", _dir)

#======== PLAYER HAS DONE A WHOLE STEP =========================================
func on_step() :
	var v_dust = fx_scene.instantiate()
	v_dust.set_type("dust",0)
	v_dust.set_position(position)
	get_parent().add_child(v_dust)

#======== PLAYER IS DEAD =======================================================
func on_die():
	emit_signal("pingmy_die")
	$content/sprite/fx_hit.start()
	travel("die")

func on_end(): emit_signal("pingmy_end")

func go_left(): g_side = 0
func go_right(): g_side = 1



