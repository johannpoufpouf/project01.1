extends CharacterBody2D
var hit_scene 		= preload("res://src/scene/hit.tscn")

@export_enum("mosquito","oil","tv") var type:	String = "mosquito"

@onready var g_animationtree = $animation/tree
@onready var g_state = g_animationtree["parameters/playback"]

var g_life = 10
var g_is_hit = false

func set_life(_life): g_life = _life

func _ready():
	g_state.travel(type)
	GLOBAL.nb_foes_alive = GLOBAL.nb_foes_alive + 1

func _physics_process(_delta):
	if GLOBAL.PLAYER!=null && g_life>0:
		var v_target = (GLOBAL.PLAYER.position - position).normalized()
		g_animationtree.set("parameters/"+type+"/blend_position", v_target.x)
		g_animationtree.set("parameters/"+type+"_hit/blend_position", v_target.x)
		g_animationtree.set("parameters/"+type+"_die/blend_position", v_target.x)
		velocity = velocity.move_toward(v_target*15, 100*_delta)
		move_and_slide()

func not_hit(): g_is_hit = false
		
func on_hit(_who, _force, _from):
	if g_is_hit : return
	g_is_hit = true

	# COMPUTE HIT VALUES
	var v_value = _force
	g_life = g_life - v_value
	
	# SHOW VALUE
	var v_hit = hit_scene.instantiate()
	v_hit.get_node("value/label").text = str(v_value)
	v_hit.position = position-Vector2(4,20)
	get_parent().add_child(v_hit)
	
	# BACK MOVE
	velocity = (position-_from).normalized()*50

	# ANIMATION
	if g_life>0: g_state.travel(type+"_hit")
	else: g_state.travel(type+"_die")
	
func on_end():
	GLOBAL.nb_foes_dead = GLOBAL.nb_foes_dead + 1
	GLOBAL.nb_foes_alive = GLOBAL.nb_foes_alive - 1
	queue_free()
	
