extends Node2D

const g_offset = [ -1, 0, 1 ]
@onready var g_maps = [ $t01, $t02, $t03, $t04, $t05, $t06, $t07, $t08, $t09 ]

var g_pos_x:int=0
var g_pos_y:int=0

# Called when the node enters the scene tree for the first time.
func _ready(): update_maps()
				
func update_maps():
	var v_pos_x = g_pos_x
	while v_pos_x<0: v_pos_x = 333 + v_pos_x
	v_pos_x = v_pos_x % 3;
	
	var v_pos_y = g_pos_y
	while v_pos_y<0: v_pos_y = 333 + v_pos_y
	v_pos_y = v_pos_y % 3;
	
	print("[DBG] maps ("+str(g_pos_x)+": "+str(v_pos_x)+") ("+str(g_pos_y)+": "+str(v_pos_y)+")")
	for y in 3:
		for x in 3:
			g_maps[(x+v_pos_x)%3+((y+v_pos_y)%3)*3].position = Vector2(
				(g_pos_x+g_offset[x])*GLOBAL.WIDTH,
				(g_pos_y+g_offset[y])*GLOBAL.HEIGHT)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if GLOBAL.PLAYER!=null:
		var v_x:int = floori(GLOBAL.PLAYER.position.x / GLOBAL.WIDTH)
		var v_y:int = floori(GLOBAL.PLAYER.position.y / GLOBAL.HEIGHT)
		if v_x!=g_pos_x || v_y!=g_pos_y:
			g_pos_x=v_x ; g_pos_y=v_y
			update_maps()
			


