extends Node

const WIDTH:  int = 640
const HEIGHT: int = 412

#========= CURRENT PLAYER ======================================================
var PLAYER = null
func get_player():
	if PLAYER == null :
		var v_player_scene = load("res://src/scene/pingmy.tscn")
		PLAYER = v_player_scene.instantiate()
	return PLAYER
	
func set_player(_player) :
	if PLAYER!=null && PLAYER!=_player: print("[ERR] set_player issue");
	PLAYER = _player

#========= DIRECTION DEFINITIONS ===============================================
enum DIR {BELOW=-2, NONE=-1, RIGHT=0, LEFT=1, UP=2, DOWN=3}
const DIRVEC = [ Vector2(1,0), Vector2(-1,0), Vector2(0,-1), Vector2(0,1)]
const DIRSTR = [ "right", "left", "up", "down"]

enum WEAPON { HOOK=1, ROCK=2, SOUND=3}

#========= RANDOMIZER ==========================================================
var RNG = RandomNumberGenerator.new()
func _ready():
	randomize()
	RenderingServer.set_default_clear_color(Color.BLACK)
	
#========= STATISTICS ==========================================================
var nb_foes_alive:int = 0
var nb_foes_dead:int = 0


