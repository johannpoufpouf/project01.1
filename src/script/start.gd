extends Node2D

@onready var g_animationtree	= $animation/tree
@onready var g_state			= g_animationtree["parameters/playback"]

signal start_game()
signal end_game()

var g_available: bool = false
var g_timeout: float = 1

func set_available()->void:	g_available = true ; $keys.visible = true
func set_disable()->void:	g_available = false; $keys.visible = false

func init(): g_state.travel("intro")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if g_timeout>0 : g_timeout = g_timeout - _delta
	if g_available :
		if Input.is_action_pressed("ui_bomb"):
			if g_timeout<=0:
				g_timeout = 0.3
				if DisplayServer.window_get_mode() != DisplayServer.WINDOW_MODE_FULLSCREEN:
					DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
				else:
					DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
		elif Input.is_anything_pressed():
			$sprite/button3.visible = true
			g_available = false
			g_state.travel("outro")
		
func on_end():
	$sprite.visible = false
	emit_signal("start_game")
